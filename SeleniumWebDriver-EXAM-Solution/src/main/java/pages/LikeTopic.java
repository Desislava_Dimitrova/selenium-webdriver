package pages;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.pages.BasePageForum;

import static org.openqa.selenium.Keys.ENTER;
import static utils.Constance.SEARCHED_TOPIC;

public class LikeTopic extends BasePageForum {
    public LikeTopic(UserActions actions, String urlKey) {
        super(actions, urlKey);
    }
    public void likeTopic(){
        actions.waitForElementVisible("forum.searchForTopic.Button");
        actions.clickElement("forum.searchForTopic.Button");
        actions.waitForElementVisible("forum.searchForTopic.searchField");
        actions.typeValueInField(SEARCHED_TOPIC ,"forum.searchForTopic.searchField" );
        actions.pressKey(ENTER);
        actions.waitForElementClickable("forum.searchForTopic.result");
        actions.clickElement("forum.searchForTopic.result");
        actions.waitForElementClickable("forum.likeButton");
        actions.clickElement("forum.likeButton");
        actions.waitForElementVisible("forum.likedButton");

    }
    public void unlikeTopic(){
        actions.waitForElementVisible("forum.searchForTopic.Button");
        actions.clickElement("forum.searchForTopic.Button");
        actions.waitForElementVisible("forum.searchForTopic.searchField");
        actions.typeValueInField(SEARCHED_TOPIC ,"forum.searchForTopic.searchField" );
        actions.pressKey(ENTER);
        actions.waitForElementClickable("forum.searchForTopic.result");
        actions.clickElement("forum.searchForTopic.result");
        actions.waitForElementClickable("forum.likedButton");
        actions.clickElement("forum.likedButton");
        actions.waitForElementVisible("forum.likeButton");
        actions.assertElementPresent("forum.likeButton");


    }
}
