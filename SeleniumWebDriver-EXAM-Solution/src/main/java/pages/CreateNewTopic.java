package pages;

import com.telerikacademy.testframework.UserActions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static utils.Constance.*;


public class CreateNewTopic extends LoginPage{
    static Date date = Calendar.getInstance().getTime();
    static DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    static String strDate = dateFormat.format(date);

    public CreateNewTopic(UserActions actions, String urlKey) {
        super(actions, "homePage");
    }

    public void createTopic(){

        actions.clickElement("//*[@id=\"create-topic\"]/span");
        actions.waitForElementVisible("forum.createTopicName.field");
        actions.typeValueInField(generateTopicTitle(), "forum.createTopicName.field");
        actions.waitForElementVisible("forum.createTopicDesc.field");
        actions.clickElement("forum.createTopicDesc.field");
        actions.typeValueInField(generateDescription()+strDate,"forum.createTopicDesc.field");
        actions.clickElement("forum.createTopic.createButton");
        actions.waitForElementVisible("forum.createdTopic.displayName");
        actions.assertElementPresent("forum.createdTopic.displayName");
    }


}
