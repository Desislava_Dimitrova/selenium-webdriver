package pages;

import com.telerikacademy.testframework.UserActions;

import static org.openqa.selenium.Keys.ENTER;
import static utils.Constance.COMMENT_SUMMARY;
import static utils.Constance.SEARCHED_TOPIC;

public class SearchAndCommentOnTopic extends LoginPage {
    public SearchAndCommentOnTopic(UserActions actions, String urlKey) {
        super(actions, urlKey);
    }

    public void searchTopicAndComment(){
        actions.waitForElementVisible("forum.searchForTopic.Button");
        actions.clickElement("forum.searchForTopic.Button");
        actions.waitForElementVisible("forum.searchForTopic.searchField");
        actions.typeValueInField(SEARCHED_TOPIC ,"forum.searchForTopic.searchField" );
        actions.pressKey(ENTER);
        actions.waitForElementClickable("forum.searchForTopic.result");
        actions.clickElement("forum.searchForTopic.result");
        actions.waitForElementClickable("forum.searchForTopic.foundTopic");
        actions.clickElement("forum.searchForTopic.foundTopic");
        actions.typeValueInField(COMMENT_SUMMARY,"forum.searchForTopic.replySummary");
        actions.clickElement("forum.replyButton");



    }

}
