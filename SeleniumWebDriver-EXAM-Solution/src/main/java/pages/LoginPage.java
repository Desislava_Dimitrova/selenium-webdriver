package pages;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.pages.BasePage;
import com.telerikacademy.testframework.pages.BasePageForum;
import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BasePageForum {
    public LoginPage(UserActions actions, String urlKey) {
        super(actions, urlKey);
    }

    public void loginUser() {
        String username = getConfigPropertyByKey("forum.users.user.username");
        String password = getConfigPropertyByKey("forum.users.user.password");

        navigateToPage();
        assertPageNavigated();

        actions.clickElement("forum.logIn.button");
        actions.waitForElementClickable("forum.userName.field");
        actions.typeValueInField(username,"forum.userName.field");
        actions.waitForElementClickable("forum.password.field");
        actions.typeValueInField(password,"forum.password.field");
        actions.waitForElementVisible("forum.loginUser.button");
        actions.clickElement("forum.loginUser.button");

    }

}
