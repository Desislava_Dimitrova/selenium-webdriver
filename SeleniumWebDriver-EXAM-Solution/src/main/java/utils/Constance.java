package utils;

import java.util.Random;

public class Constance {

    private static final Random rand = new Random(System.currentTimeMillis());


    public static final String SEARCHED_TOPIC = "Alpha 38 QA - We are awesome and great";
    public static final String COMMENT_SUMMARY = "Hi, I am Desy, and I did it :grinning: :partying_face: :beers: " +
            ":see_no_evil: :hear_no_evil: :speak_no_evil: :heart:";

   public static String generateTopicTitle() {
       String title = new String();
       int n = rand.nextInt(250);
       n += 5;
       title = String.valueOf(n);
       for (int i = 0 ; i < n ; i ++) {
           if (i % 5 == 0) {
               title += " ";
           } else {
               title += generateRandomCharacter();
           }
       }

       return title;
   }
    public static String generateDescription() {
        String description ="This is my first created topic\n";
        int n = rand.nextInt(20);
        n = 100 + n;


        for (int i = 0 ; i < n ; i ++) {
            if (i % 5 == 0) {
                description += " ";
            } else {
                description = description + generateRandomCharacter();
            }
        }
;
        return description;
    }

    private static String generateRandomCharacter() {
        char c = (char)(rand.nextInt(26) + 'a');
        return "" + c;
    }
}
