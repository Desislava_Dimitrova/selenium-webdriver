package com.telerikacademy.testframework.pages;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class BasePageForum {

    protected String url;
    public UserActions actions;

    public BasePageForum(UserActions actions, String urlKey) {
        String pageUrl = Utils.getConfigPropertyByKey(urlKey);
        this.actions = actions;
        this.url = pageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void navigateToPage() {
        System.out.println("navigateToPage url=" + url);

        this.actions.getDriver().get(url);
    }

    public void assertPageNavigated() {
        String currentUrl = actions.getDriver().getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url,
                currentUrl.contains(url));
    }


}
