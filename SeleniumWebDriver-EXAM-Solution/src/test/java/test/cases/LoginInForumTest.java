package test.cases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.LoginPage;

public class LoginInForumTest {
    static UserActions actions;

    @BeforeClass
    public static void setUp() {
        actions = new UserActions();
        UserActions.loadBrowser("homePage");
    }

//    @AfterClass
//    public static void tearDown() {
//
//        actions.getDriver().quit();
//    }

    @Test
    public void testLogin(){
        login();
        actions.assertElementPresent("forum.userProfile");
    }

    private void login(){
        LoginPage login = new LoginPage(actions,"homePage");
        login.loginUser();

    }
}
