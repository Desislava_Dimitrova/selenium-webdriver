package test.cases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.CreateNewTopic;
import pages.LikeTopic;
import pages.LoginPage;

public class LikeTopicTest {
    static UserActions actions;

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("homePage");
        actions = new UserActions();
    }

    @AfterClass
    public static void tearDown() {
        //UserActions.quitDriver();
        actions.getDriver().quit();
    }

    @Test
    public void likeTopicTest(){
        LoginPage login = new LoginPage(actions,"homePage");
        login.loginUser();
        LikeTopic likeTopic= new LikeTopic(actions, "homePage");
        likeTopic.likeTopic();

    }
}
