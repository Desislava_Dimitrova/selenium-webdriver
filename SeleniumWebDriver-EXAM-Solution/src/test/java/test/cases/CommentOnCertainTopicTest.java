package test.cases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.LoginPage;
import pages.SearchAndCommentOnTopic;

public class CommentOnCertainTopicTest {
    static UserActions actions;

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("homePage");
        actions = new UserActions();
    }
    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }

    @Test
    public void createTopicTest(){
        LoginPage login = new LoginPage(actions,"homePage");
        login.loginUser();
        SearchAndCommentOnTopic searchForTopic = new SearchAndCommentOnTopic(actions,"homePage");
        searchForTopic.searchTopicAndComment();

    }
}
