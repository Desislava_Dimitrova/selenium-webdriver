package test.cases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.CreateNewTopic;
import pages.LoginPage;

public class CreateTopicTest {
    static UserActions actions;

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("homePage");
        actions = new UserActions();
    }

    @AfterClass
    public static void tearDown() {
        actions.getDriver().quit();
    }

    @Test
    public void createTopicTest(){
        LoginPage login = new LoginPage(actions,"homePage");
        login.loginUser();
        CreateNewTopic createTopic = new CreateNewTopic(actions, "homePage");
        createTopic.createTopic();

    }
}
